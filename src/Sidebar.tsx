import React from "react";
import { iconFor, opposite } from "./boardState";
import { GAME_STATE, canStartGame } from "./gameState";
import { COLOR, PIECE } from "./primitives";

export type SetPlayerColor = (color: COLOR) => void;
export type StartGame = () => Promise<void>;
export type OnPromote = (kind: PIECE) => Promise<void>;

export interface SidebarProps {
  gameState: GAME_STATE;
  playerColor: COLOR;
  onPromote?: OnPromote;
  setPlayerColor: SetPlayerColor;
  startGame: StartGame;
}

export interface ButtonListProps {
  gameState: GAME_STATE;
  oppositeColor: COLOR;
  startGame: StartGame;
  switchTo: () => void;
}

export interface ButtonProps {
  children?: React.ReactNode;
  onClick?: (() => void) | (() => Promise<void>);
}

export interface PromoteListProps {
  color: COLOR;
  onPromote?: OnPromote;
}

export interface PromotionProps {
  color: COLOR;
  kind: PIECE;
  onClick?: (() => void) | (() => Promise<void>);
}

const VALID_PROMOTIONS: PIECE[] = ["rook", "knight", "bishop", "queen"];

const PromoteList = ({
  color,
  onPromote,
}: PromoteListProps): JSX.Element | null => {
  if (typeof onPromote === "undefined") {
    return null;
  }

  return (
    <div className="promote-section">
      <h3>Promote Pawn</h3>
      <ol className="promote-list">
        {VALID_PROMOTIONS.map((piece) => (
          <li key={piece}>
            <Promotion
              color={color}
              kind={piece}
              onClick={() => onPromote(piece)}
            />
          </li>
        ))}
      </ol>
    </div>
  );
};

const Promotion = ({ color, kind, onClick }: PromotionProps): JSX.Element => {
  return (
    <div className="promotion">
      <button onClick={onClick}>
        <span className="promotion-icon">{iconFor({ kind, color })}</span>
      </button>
    </div>
  );
};

const ButtonList = ({
  gameState,
  oppositeColor,
  startGame,
  switchTo,
}: ButtonListProps): JSX.Element => {
  return (
    <div className="button-section">
      <h3>Options</h3>
      <ol className="button-list">
        {canStartGame(gameState) ? (
          <Button onClick={startGame}>Start Game</Button>
        ) : (
          <Button onClick={switchTo}>Switch to {oppositeColor}</Button>
        )}
      </ol>
    </div>
  );
};

const Button = ({ children, onClick }: ButtonProps): JSX.Element => {
  return (
    <div className="button">
      <button onClick={onClick}>{children}</button>
    </div>
  );
};

const Sidebar = ({
  gameState,
  playerColor,
  onPromote,
  setPlayerColor,
  startGame,
}: SidebarProps): JSX.Element => {
  const oppositeColor = opposite(playerColor);
  const switchTo = () => setPlayerColor(oppositeColor);

  return (
    <div className="sidebar">
      <ButtonList
        gameState={gameState}
        oppositeColor={oppositeColor}
        startGame={startGame}
        switchTo={switchTo}
      />
      <PromoteList color={playerColor} onPromote={onPromote} />
    </div>
  );
};

export default Sidebar;
