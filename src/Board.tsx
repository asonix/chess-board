import React from "react";
import BoardState, { Coordinates, PieceState, iconFor } from "./boardState";
import { GAME_STATE, canMakeMove } from "./gameState";
import { COLOR, FILE, PIECE, RANK } from "./primitives";

export type Selected = {
  piece: PieceState;
  location: Coordinates;
};

export type OnSelectPiece = (selected: Selected) => void;
export type OnMovePiece = (
  selected: Selected,
  destination: Coordinates
) => Promise<void>;
export type OnSelectDestination = (destination: Coordinates) => Promise<void>;

export interface BoardProps {
  boardState: BoardState;
  gameState: GAME_STATE;
  onMovePiece: OnMovePiece;
  playerColor: COLOR;
}

export interface CellProps {
  boardState: BoardState;
  gameState: GAME_STATE;
  playerColor: COLOR;
  rank: RANK;
  file: FILE;
  selected: Selected | null;
  onSelectPiece: OnSelectPiece;
  onSelectDestination: OnSelectDestination;
}

const RANKS: RANK[] = [1, 2, 3, 4, 5, 6, 7, 8];
const FILES: FILE[] = ["a", "b", "c", "d", "e", "f", "g", "h"];
const PIECES: PIECE[] = ["pawn", "rook", "knight", "bishop", "queen", "king"];
const COLORS: COLOR[] = ["white", "black"];

const Cell = ({
  boardState,
  gameState,
  playerColor,
  rank,
  file,
  selected,
  onSelectDestination,
  onSelectPiece,
}: CellProps): JSX.Element => {
  const filed = boardState[file];

  const onEmptyClick = async () => {
    return await onSelectDestination({ rank, file });
  };

  if (typeof filed === "undefined") {
    return <td className="icon-cell" onClick={onEmptyClick}></td>;
  }

  const piece = filed[rank];

  if (typeof piece === "undefined") {
    return <td className="icon-cell" onClick={onEmptyClick}></td>;
  }

  const onClick = () => {
    const selected: Selected = {
      piece: { ...piece },
      location: {
        file,
        rank,
      },
    };

    return onSelectPiece(selected);
  };

  const maybeOnClick =
    piece.color === playerColor && canMakeMove(playerColor, gameState)
      ? onClick
      : onEmptyClick;

  const classNames =
    selected !== null &&
    selected.piece.kind === piece.kind &&
    selected.piece.color === piece.color &&
    selected.location.file === file &&
    selected.location.rank === rank
      ? ["icon-cell", "selected"]
      : ["icon-cell"];

  return (
    <td className={classNames.join(" ")} onClick={maybeOnClick}>
      <span className="icon">{iconFor(piece)}</span>
    </td>
  );
};

const Board = ({
  boardState,
  gameState,
  onMovePiece,
  playerColor,
}: BoardProps): JSX.Element => {
  const [selected, setSelected] = React.useState<Selected | null>(null);

  const onSelectDestination = async (
    destination: Coordinates
  ): Promise<void> => {
    if (selected === null) {
      return;
    }

    await onMovePiece(selected, destination);

    setSelected(null);
  };

  const files = [...FILES];
  const ranks = [...RANKS];

  if (playerColor === "white") {
    ranks.reverse();
  } else {
    files.reverse();
  }

  return (
    <div className="board">
      <table className="chess-board">
        <tbody>
          <tr>
            <td></td>
            {files.map((file) => (
              <td key={file}>{file.toUpperCase()}</td>
            ))}
            <td></td>
          </tr>
          {ranks.map((rank) => (
            <tr key={rank}>
              <td>{rank}</td>
              {files.map((file) => (
                <Cell
                  key={file}
                  boardState={boardState}
                  gameState={gameState}
                  playerColor={playerColor}
                  file={file}
                  rank={rank}
                  selected={selected}
                  onSelectDestination={onSelectDestination}
                  onSelectPiece={setSelected}
                />
              ))}
              <td>{rank}</td>
            </tr>
          ))}
          <tr>
            <td></td>
            {files.map((file) => (
              <td key={file}>{file.toUpperCase()}</td>
            ))}
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Board;
export { PIECES, COLORS, FILES, RANKS };
