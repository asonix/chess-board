import GAME_STATE from "./gameState";
import BoardWireFormat from "./boardWireFormat";

export type BoardResponse = {
  boardState: BoardWireFormat;
  gameState: GAME_STATE;
};

export default BoardResponse;
