import { COLOR } from "./primitives";

export type GAME_STATE =
  | "pending"
  | "white"
  | "black"
  | "win"
  | "lose"
  | "draw";

const canStartGame = (gameState: GAME_STATE): boolean => {
  return (
    gameState === "pending" ||
    gameState === "win" ||
    gameState === "lose" ||
    gameState === "draw"
  );
};

const canMakeMove = (playerColor: COLOR, gameState: GAME_STATE): boolean => {
  return (playerColor as GAME_STATE) === gameState;
};

const toColor = (gameState: GAME_STATE): COLOR | null => {
  switch (gameState) {
    case "white":
      return "white";
    case "black":
      return "black";
    default:
      return null;
  }
};

export default GAME_STATE;
export { canMakeMove, canStartGame, toColor };
