export type COLOR = "black" | "white";
export type PIECE = "pawn" | "rook" | "knight" | "bishop" | "queen" | "king";
export type FILE = "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h";
export type RANK = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8;
