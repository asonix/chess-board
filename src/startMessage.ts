import BoardWireFormat from "./boardWireFormat";

export type StartMessage = {
  board: BoardWireFormat;
  gameId: string;
};

export default StartMessage;
