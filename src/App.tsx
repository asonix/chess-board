import React from "react";
import { Coordinates } from "./boardState";
import { GAME_STATE, toColor } from "./gameState";
import Board, { Selected } from "./Board";
import BoardState from "./boardState";
import Sidebar from "./Sidebar";
import { COLOR, PIECE } from "./primitives";
import { init } from "./client";
import "./App.css";

type PromoteState = {
  selected: Selected;
  destination: Coordinates;
};

const api = init("http://localhost:8000");

const INITIAL_STATE: BoardState = {};

const App = (): JSX.Element => {
  const [boardState, setBoardState] = React.useState(INITIAL_STATE);
  const [gameId, setGameId] = React.useState<string | null>(null);
  const [playerColor, setPlayerColor] = React.useState<COLOR>("white");
  const [gameState, setGameState] = React.useState<GAME_STATE>("pending");
  const [promoteState, setPromoteState] = React.useState<PromoteState | null>(
    null
  );

  const startGame = React.useCallback(async () => {
    const result = await api.startGame(playerColor);

    if (result === null) {
      return;
    }

    const [gameId, newBoardState] = result;

    setGameState("white");
    setGameId(gameId);
    setBoardState(newBoardState);
  }, [playerColor, setBoardState, setGameState, setGameId]);

  const pollLoop = React.useCallback(async () => {
    if (gameId === null) {
      throw new Error("Game ID is null");
    }

    const boardResponse = await api.poll(gameId);

    if (boardResponse === null) {
      return;
    }

    const color = toColor(boardResponse.gameState);
    if (color !== null) {
      setPlayerColor(color);
    }

    setBoardState(boardResponse.boardState);
    setGameState(boardResponse.gameState);
  }, [gameId, setBoardState]);

  const onPromote = React.useCallback(
    async (kind: PIECE) => {
      if (promoteState === null || gameId === null) {
        return;
      }

      const { selected, destination } = promoteState;

      setPromoteState(null);
      await api.makeMove(selected, destination, gameId, kind);
    },
    [gameId, promoteState, setPromoteState]
  );

  const onMovePiece = React.useCallback(
    async (selected: Selected, destination: Coordinates) => {
      if (gameId === null) {
        return;
      }

      const promoteRank = {
        black: 1,
        white: 8,
      };

      const canPromote =
        selected.piece.kind === "pawn" &&
        destination.rank === promoteRank[playerColor];

      if (canPromote) {
        setPromoteState({ selected, destination });
      } else {
        await api.makeMove(selected, destination, gameId);
      }
    },
    [gameId, playerColor, setPromoteState]
  );

  React.useEffect(() => {
    console.log("new poll loop");
    (function loop() {
      void Promise.resolve()
        .then(async () => await pollLoop())
        .then(loop, () => null);
    })();
  }, [pollLoop]);

  const maybeOnPromote = promoteState !== null ? onPromote : undefined;

  return (
    <div className="App">
      <Sidebar
        gameState={gameState}
        playerColor={playerColor}
        onPromote={maybeOnPromote}
        setPlayerColor={setPlayerColor}
        startGame={startGame}
      />
      <Board
        gameState={gameState}
        boardState={boardState}
        onMovePiece={onMovePiece}
        playerColor={playerColor}
      />
    </div>
  );
};

export default App;
