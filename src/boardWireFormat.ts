import BoardState from "./boardState";
import { COLOR, PIECE, RANK, FILE } from "./primitives";

export type BoardWireFormat = PieceWireFormat[];
export type PieceWireFormat = [FILE, RANK, PIECE, COLOR];

export const toBoardState = (boardWireFormat: BoardWireFormat): BoardState =>
  boardWireFormat.reduce(
    (currentState, [file, rank, kind, color]) => ({
      ...currentState,
      [file]: {
        ...currentState[file],
        [rank]: {
          kind,
          color,
        },
      },
    }),
    {} as BoardState
  );

export default BoardWireFormat;
